﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class CentralProcessingUnit
  {
    public string Make { get; set; }
    public string Model { get; set; }
    public decimal ClockSpeed { get; set; }
  }
}
