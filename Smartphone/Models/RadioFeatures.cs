﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class RadioFeatures
  {
    public bool HasGps {get; set;}
    public bool HasWifi {get; set;}
    public bool HasLte {get; set;}
    public bool HasCdma { get; set; }

  }
}
