﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Screen
  {
    /// <summary>
    /// Type of screen
    /// </summary>
    public Lookups.ScreenTypes ScreenType { get; set; }

    public void SetScreenType(int screenType)
    {
      ScreenType = (Lookups.ScreenTypes)screenType;
    }

    public int GetScreenType()
    {
      return (int)ScreenType;
    }
    
    /// <summary>
    /// True if screen is a touch screen
    /// </summary>
    public bool IsTouchScreen { get; set; }

    public int ResolutionWidth {get; set;}
    public int ResolutionHeight {get; set;}
    public int PixelsPerInch {get; set;}
    /// <summary>
    /// diagonal size measures in inches
    /// </summary>
    public decimal Size { get; set;}
    
  }
}
