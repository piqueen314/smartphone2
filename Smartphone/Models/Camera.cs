﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class Camera
  {
    public bool HasFlash {get; set;}
    public int ResolutionWidth {get; set;}
    public int ResolutionHeight { get; set; }

  }
}
