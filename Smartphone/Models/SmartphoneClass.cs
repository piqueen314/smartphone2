﻿using Smartphone.Models;
using System.Collections.Generic;
using System.Text;
using Smartphone.Utility;
using System;
namespace Smartphone.Models
{
  /// <summary>
  /// our sweet smartphone class
  /// </summary>
  public class SmartphoneClass
  {
    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Phone's diagonal screen size in inches.
    /// </summary>
    public decimal ScreenSizeIn { get; set; }

    /// <summary>
    /// Read-only screen size in mm.
    /// </summary>
    public decimal ScreenSizeMm
    {
      get
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }
       
    /// <summary>
    /// Screen width in pixels.
    /// </summary>
    public int ScreenWidth { get; set; }

    /// <summary>
    /// Screen height in pixels.
    /// </summary>
    public int ScreenHeight { get; set; }

    /// <summary>
    /// Front camera megapixels.
    /// </summary>
    public decimal FrontCameraMegapixels { get; set; }

    /// <summary>
    /// Rear camera megapixels.
    /// </summary>
    public decimal RearCameraMegapixels { get; set; }
    /// <summary>
    /// weight in onces
    /// </summary>
    public decimal Weight {get; set;}

    public bool IsWaterResistant {get;set;}

    public List<Apps> AppList {get; set;}
    public Chassis Chassis {get; set;} 
    public CentralProcessingUnit CPU {get; set;}
    public Ram RAM {get; set;}
    public Camera FrontCamera {get; set;}
    public Camera BackCamera {get; set;}
    public OperatingSys OS {get;set;}
    public RadioFeatures RadioFeatures{get; set;}
    public Screen Screen {get; set;}
    public Battery Battery {get;set;}
  
  public override string ToString()
  {
    var sb= new StringBuilder();
    sb.AppendFormat("********app List: ******");
    foreach (var x in this.AppList)
    {
      sb.AppendFormat("\r\n{0}",x.Name);
    }
    sb.AppendFormat("\r\n******** Model Info*********");
    sb.AppendFormat("\r\nMake {0}",this.Make);
    sb.AppendFormat("\r\nModel {0}",this.Model);
    sb.AppendFormat("\r\n******Ram info ************");
    sb.AppendFormat("\r\nRam: {0}", this.RAM.Size);
    Console.WriteLine("\r\n*****Operating system*****");
    Printer.Print("OS :", this.OS.Name);
    Printer.Print("OS version",this.OS.Version);
    sb.AppendFormat("\r\n******Chassis ************");
    sb.AppendFormat("\r\nHeight:",this.Chassis.Height);
    sb.AppendFormat("\r\nLength:", this.Chassis.Length);
    sb.AppendFormat("\r\nWidth:", this.Chassis.Width);
    sb.AppendFormat("******** Material List: ******");
    foreach (var x in this.Chassis.MaterialType)
    {
      sb.AppendFormat("\r\n{0}", x);
    }
    return sb.ToString();

  }
}
}


