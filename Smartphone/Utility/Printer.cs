﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Utility
{
  public static class Printer
  {
    public static void Print(string label, string text)
    {
      Console.WriteLine("{0}: {1}",label,text);
    }
    
  }
}
