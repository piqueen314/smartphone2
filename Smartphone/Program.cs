﻿using Smartphone.Models;
using Smartphone.Utility;
using System;
using System.Collections.Generic;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      //instance of smartphone class
      
      var myList = new List<object>();
      var sc = new SmartphoneClass();

      //Printer.Print("Foo", "Boo");
      ////set all the properties
      //sc.FrontCameraMegapixels = (decimal)1.3;
      //sc.Make = "LG";
      //sc.Model = "Nexus 5";
      //sc.RearCameraMegapixels = 8;
      //sc.ScreenHeight = 1920;
      //sc.ScreenSizeIn = (decimal)4.95;
      //sc.ScreenWidth = 1080;

      //set Apps List
      sc.AppList = new List<Apps>();
      sc.AppList.Add(new Apps("Facebook"));
      sc.AppList.Add(new Apps("Twitter"));
      sc.AppList.Add(new Apps("Snap Chat"));

      //set Screen Type
      sc.Screen = new Screen();

      sc.Screen.ScreenType = Lookups.ScreenTypes.Amoled;
      sc.Screen.SetScreenType((int)Lookups.ScreenTypes.Amoled);
      sc.Screen.IsTouchScreen = true;
      sc.Screen.PixelsPerInch = 300;
      sc.Screen.ResolutionHeight = 700;
      sc.Screen.ResolutionWidth = 1200;
      sc.Screen.Size = 9.3m;

      //set Battery
      sc.Battery = new Battery();
      sc.Battery.IsRemovable = true;
      sc.Battery.KwH = 3;
      sc.Battery.MaxTalkTime = 100;
      //set Camera 
      sc.BackCamera = new Camera();
      sc.FrontCamera = new Camera();
      
      sc.FrontCamera.HasFlash = true;
      sc.FrontCamera.ResolutionHeight = 344;
      sc.FrontCamera.ResolutionWidth = 1200;

      sc.BackCamera.HasFlash = false;
      sc.BackCamera.ResolutionHeight = 900;
      sc.FrontCamera.ResolutionWidth = 1300;

      //set central processing unit
      sc.CPU = new CentralProcessingUnit();
      sc.CPU.Make="ARM";
      sc.CPU.Model = "3400";
      sc.CPU.ClockSpeed = 23.5m;

      //set chassis
      sc.Chassis = new Chassis();
      sc.Chassis.Height = 23.2m;
      sc.Chassis.Length = 12.3m;
      sc.Chassis.MaterialType = new List<Lookups.MaterialTypes>();
      sc.Chassis.MaterialType.Add(Lookups.MaterialTypes.Aluminum);
      sc.Chassis.MaterialType.Add(Lookups.MaterialTypes.Glass);

      //set operating system
      sc.OS = new OperatingSys();
      sc.OS.Name = "Android";
      sc.OS.Version = "Kit Kat";

      //set Radio Features
      sc.RadioFeatures = new RadioFeatures();
      sc.RadioFeatures.HasCdma = true;
      sc.RadioFeatures.HasGps = true;
      sc.RadioFeatures.HasLte = false;
      sc.RadioFeatures.HasWifi = true;

      //set Ram
      sc.RAM = new Ram();
      sc.RAM.Size = 3.4m;
      //
      sc.Make = "samsung";
      sc.Model = "nexus7";
      //foreach(var x in Lookups.ScreenTypes)
      //{
      //  Console.WriteLine(x);
      //}
      //write out phone details
      Console.WriteLine(sc.ToString());
      Console.WriteLine("**** Press the AnyKey ****");
      Console.ReadKey();

      //write the phone details out to the console
      //Console.WriteLine("**** My Smartphone ****");
      //Console.WriteLine();
      //Console.WriteLine("Make: {0}, Model: {1}", sc.Make, sc.Model);
      //Console.WriteLine("Screen Size: {0} in. ({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      //Console.WriteLine("Screen Resolution: {0} x {1} pixels", sc.ScreenHeight, sc.ScreenWidth);
      //Console.WriteLine("Rear Camera: {0} MP", sc.RearCameraMegapixels);
      //Console.WriteLine("Front Camera: {0} MP", sc.FrontCameraMegapixels);
      //Console.WriteLine();
      //Console.WriteLine("**** Press the AnyKey ****");
      //Console.ReadKey();
    }
  }
}
